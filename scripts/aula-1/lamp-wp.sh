#!/bin/sh
# Filename: lamp-wp.sh

# Instalação e configuração em https://wiki.debian.org/WordPress#Installation_-_Debian_9_.28Stretch.29_and_10_.28Buster.29

apt update -qq && apt -y -qq upgrade

apt install -y -qq wordpress curl apache2 mariadb-server