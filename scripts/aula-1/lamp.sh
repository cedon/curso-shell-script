#!/bin/sh
# Filename: lamp.sh

# atualizar lista de repositórios e pacotes instalados
apt update -qq && apt -y -qq upgrade

# instalar php7.0
apt install -y -qq php7.0-fpm php7.0-mbstring php-gettext php-pear php7.0-mysql php7.0-gd php7.0-mcrypt php7.0-pdo php7.0-xml

# instalar mariadb
apt install -y -qq mariadb-server

# instalar apache2
apt install -y -qq apache2


