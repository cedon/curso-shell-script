#!/bin/sh
# Filename: lamp-wp-config.sh

# Instalação e configuração em https://wiki.debian.org/WordPress#Installation_-_Debian_9_.28Stretch.29_and_10_.28Buster.29
clear

echo
echo
echo
echo "=========================instalando a infraestrutura==========================="
##atualizando
apt update -qq && apt -y -qq upgrade
##instalando LAMP+WP
apt install -y -qq --reinstall wordpress curl apache2 mariadb-server

echo "=========================configurando os serviços ==========================="
echo "=========================configurando o MariaDB ==========================="
#Fazendo a configuração de segurança do MARIADB
mysql_secure_installation

echo "=========================configurando VirtualHost ==========================="
#Deletar VirtualHosts Antigos
rm /etc/apache2/sites-available/000-default.conf

#Configurar VirtualHost
touch /etc/apache2/sites-available/000-default.conf 
echo '<VirtualHost *:80>' >> /etc/apache2/sites-available/000-default.conf
echo '  DocumentRoot /usr/share/wordpress' >> /etc/apache2/sites-available/000-default.conf
echo '  Alias /wp-content /var/lib/wordpress/wp-content' >> /etc/apache2/sites-available/000-default.conf
echo '  <Directory /usr/share/wordpress>' >> /etc/apache2/sites-available/000-default.conf
echo '      Options FollowSymLinks' >> /etc/apache2/sites-available/000-default.conf
echo '      AllowOverride Limit Options FileInfo' >> /etc/apache2/sites-available/000-default.conf
echo '      DirectoryIndex index.php' >> /etc/apache2/sites-available/000-default.conf
echo '      Require all granted' >> /etc/apache2/sites-available/000-default.conf
echo '  </Directory>' >> /etc/apache2/sites-available/000-default.conf
echo '  <Directory /var/lib/wordpress/wp-content>' >> /etc/apache2/sites-available/000-default.conf
echo '      Options FollowSymLinks' >> /etc/apache2/sites-available/000-default.conf
echo '      Require all granted' >> /etc/apache2/sites-available/000-default.conf
echo '  </Directory>' >> /etc/apache2/sites-available/000-default.conf
echo '  ErrorLog ${APACHE_LOG_DIR}/error.log' >> /etc/apache2/sites-available/000-default.conf
echo '  CustomLog ${APACHE_LOG_DIR}/access.log combined' >> /etc/apache2/sites-available/000-default.conf
echo '</VirtualHost>' >> /etc/apache2/sites-available/000-default.conf

#Reinicir o Servidor Web
systemctl restart apache2

echo "=========================configurando o WordPress ==========================="
#Criar o arquivo de configuração do WordPress
rm /etc/wordpress/config-default.php
touch /etc/wordpress/config-default.php
echo "<?php" >> /etc/wordpress/config-default.php
echo "define('DB_NAME', 'wordpress');" >> /etc/wordpress/config-default.php
echo "define('DB_USER', 'wordpress');" >> /etc/wordpress/config-default.php
echo "define('DB_PASSWORD', '123');" >> /etc/wordpress/config-default.php
echo "define('DB_HOST', 'localhost');" >> /etc/wordpress/config-default.php
echo "define('WP_CONTENT_DIR', '/var/lib/wordpress/wp-content');" >> /etc/wordpress/config-default.php
echo "?>" >> /etc/wordpress/config-default.php

#Criar o sql do banco de dados do WordPress
rm /tmp/wp.sql
rm /tmp/drop.sql
touch /tmp/wp.sql
touch /tmp/drop.sql
echo "DROP DATABASE wordpress;" >> /tmp/drop.sql
echo "CREATE DATABASE wordpress;" >> /tmp/wp.sql
echo "GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,DROP,ALTER \
ON wordpress.* \
TO 'wordpress'@'localhost' \
IDENTIFIED BY '123';" >> /tmp/wp.sql
echo "UPDATE mysql.user SET plugin='' WHERE User='root';" >> /tmp/wp.sql
echo "FLUSH PRIVILEGES;" >> /tmp/wp.sql

#Apagar antigo Banco de Dados
cat /tmp/drop.sql | mysql --defaults-extra-file=/etc/mysql/debian.cnf
#Criar novo Banco de Dados
cat /tmp/wp.sql | mysql --defaults-extra-file=/etc/mysql/debian.cnf

rm /tmp/wp.sql
rm /tmp/drop.sql
#Reinicia o serviço do MariaDB
systemctl restart mariadb

echo "FIM"