#!/bin/bash
# Filename: backup.sh
# Faz backup de um diretório


echo -e " \033[1;33m Digite o caminho completo da origem.: \033[0m  "
read DIR_ORIGEM

clear

echo -e " \033[1;34m Digite o caminho completo do destino.: \033[0m "
read DIR_DESTINO

clear

verifica_argumentos(){

	if [ $# -lt 1 ];
	then
	   echo "Faltou informar um dos argumentos necessarios!"
	   exit 1
	fi
}

copia_arquivos(){

	verifica_argumentos

	clear

	echo "Realizando backup..."

	#Verificando se o dir de destino existe

	if ! [ -d $DIR_DESTINO ]
	then
		mkdir $DIR_DESTINO
		echo "Diretorio de Destino Criado"
	fi


	#COPIANDO ARQUIVOS

	for arq in `ls $DIR_ORIGEM`
	do
		cp /$DIR_ORIGEM/$arq $DIR_DESTINO/$arq.bak
	done	

}

copia_arquivos