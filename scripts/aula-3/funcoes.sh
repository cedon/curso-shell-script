##!/bin/bash
# Filename: funcoes.sh

function versao_debian(){
	cat /etc/debian_version
}

CheckUser () {
   USER_ID=$(/usr/bin/id -u)
   return $USER_ID
}
 
echo "Carregando script..."
 
CheckUser
if [ $? -ne "0" ]; then
   echo -e "nVoce não é root, execute como super-usuario!n"
   exit 1
fi
 
# Macete para imprimir saida de funcao dentro do echo
echo -e "SO:t$(versao_debian)”
